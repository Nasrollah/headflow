__author__ = "Martin Alnaes <martinal@simula.no> and Oeyvind Evju <oyvinev@simula.no>"
__date__ = "2013-06-25"
__copyright__ = "Copyright (C) 2013 " + __author__
__license__  = "GNU GPL version 3 or any later version"

from .paramdict import ParamDict
from .parameterized import Parameterized
from ..postprocessing import field_classes, PPField
from ..postprocessing import * # TODO: Remove this, want to know what dependencies we have here to avoid spaghetti...
from .utils_pyminifier import minify
from .utils import cbcflow_warning, cbcflow_print, hdf5_link, safe_mkdir, timeit

from dolfin import Function, MPI, plot, File, project, as_vector, HDF5File, XDMFFile, error

import os, re, inspect, pickle, shelve
from collections import defaultdict

# TODO: Extract a Plotter class and a Storage class to separate this logic


# Need this for safe .txt file storage
if MPI.num_processes() > 1:
    on_master_node = MPI.process_number() == 0
else:
    on_master_node = 1


# Disable all plotting if we run in parallell
if MPI.num_processes() > 1:
    cbcflow_warning("Unable to plot dolfin plots in paralell. Disabling.")
    disable_plotting = True
else:
    disable_plotting = False


# Set up pylab if available
if disable_plotting:
    pylab = None
else:
    try:
        import pylab
        pylab.ion()
    except:
        pylab = None
        cbcflow_warning("Unable to load pylab. Disabling pylab plotting.")


# Enable dolfin plotting if environment variable DISPLAY is set
if disable_plotting:
    dolfin_plotting = False
else:
    if 'DISPLAY' in os.environ:
        dolfin_plotting = True
    else:
        cbcflow_warning("Did not find display. Disabling dolfin plotting.")
        dolfin_plotting = False


class DependencyException(Exception):
    def __init__(self, fieldname=None, dependency=None, timestep=None, original_exception_msg=None):
        message = []
        if fieldname:
            message += ["Dependency/dependencies not found for field %s." % fieldname]
        if dependency:
            message += ["Dependency %s not functioning." % dependency]
        if timestep:
            message += ["Relative timestep is %d. Are you trying to calculate time-derivatives at t=0?" % (name, timestep)]
        if original_exception_msg:
            message += ["\nOriginal exception was: " + original_exception_msg]
        message = ' '.join(message)
        Exception.__init__(self, message)

# Fields available through pp.get(name) even though they have no PPField class
builtin_fields = ("t", "timestep")

class NSPostProcessor(Parameterized):
    def __init__(self, params=None):
        Parameterized.__init__(self, params)

        # Storage of actual fields
        self._fields = {}

        # Representations of field dependencies
        self._sorted_fields_keys = [] # Topological ordering of field names
        self._dependencies = {} # Direct dependencies dep[name] = ((depname0, ts0), (depname1, ts1), ...)
        self._full_dependencies = {} # Indirect dependencies included
        for depname in builtin_fields:
            self._dependencies[depname] = []
            self._full_dependencies[depname] = []
        #self._reverse_dependencies = {} # TODO: Need this?

        # Plan of what to compute now and in near future
        self._plan = defaultdict(lambda: defaultdict(int))

        # Cache of computed values needed for planned computations
        self._cache = defaultdict(dict)

        # Keep track of how many times .get has called each field.compute, for administration:
        self._compute_counts = defaultdict(int) # Actually only used for triggering "before_first_compute"

        # Keep track of last (time, timestep) computation of each field was triggered directly
        self._last_trigger_time = defaultdict(lambda: (-1e16,-1e16))

        # Caches for file storage
        self._save_counts = defaultdict(int)
        self._datafile_cache = {}

        # Cache for plotting
        self._plot_cache = {}

        # Callback to be called with fields where the 'callback' action is enabled
        # Signature: ppcallback(field, data, t, timestep)
        self._callback = None

        # Hack to make these objects available throughout during update... Move these to a struct?
        self._problem = None
        self._spaces = None
        self._solution = None

    @classmethod
    def default_params(cls):
        params = ParamDict(
            casedir=".",
            enable_timer=False,
            )
        return params

    def _insert_in_sorted_list(self, fieldname):
        # Topological ordering of all fields, so that all dependencies are taken care of

        # If already in list, assuming there's no change to dependencies
        if fieldname in self._sorted_fields_keys:
            return

        # Find largest index of dependencies in sorted list
        deps = [dep[0] for dep in self._dependencies[fieldname] if dep[0] not in builtin_fields]
        max_index = max([-1]+[self._sorted_fields_keys.index(dep) for dep in deps])

        # Insert item after all its dependencies
        self._sorted_fields_keys.insert(max_index+1, fieldname)

    def find_dependencies(self, field):
        "Read dependencies from source (if available) from calls to PostProcessor.get command"

        # Get source
        s = inspect.getsource(field.compute)
        s = minify(s) # Removes all comments, empty lines etc.

        # Remove comment blocks
        s = s.split("'''")
        s = s[0::2]
        s = ''.join(s)
        s = s.split('"""')
        s = s[0::2]
        s = ''.join(s)
        s = minify(s)

        # Get argument names for the compute function
        args = inspect.getargspec(field.compute)[0]
        self_arg = args[0]
        pp_arg = args[1]

        # Read the code for dependencies
        deps = []
        deps_raw = re.findall(pp_arg+".get\((.+)\)", s)
        for dep in deps_raw:
            # Split into arguments (name, timestep)
            dep = dep.split(',')

            # Append default 0 if dependent timestep not specified
            if len(dep) == 1:
                dep.append(0)

            # Convert timestep to int
            dep[1] = int(dep[1])

            # Get field name from string literal or through string variable
            dep[0] = dep[0].strip(' ').replace('"', "'")
            if "'" in dep[0]:
                # If pp.get('Velocity')
                dep[0] = dep[0].replace("'","")
            else:
                # If pp.get(self.somevariable), get the string hiding at self.somevariable
                dep[0] = eval(dep[0].replace(self_arg, "field", 1))

                # TODO: Test alternative solution without eval (a matter of principle) and with better check:
                #s, n = dep[0].split(".")
                #assert s == self_arg, "Only support accessing strings through self."
                #dep[0] = getattr(field, n)

                # TODO: Possible to get variable in other cases through more introspection?
                #       Probably not necessary, just curious.

            # Append to dependencies
            deps.append(tuple(dep))

        # Make unique (can happen that deps are repeated in rare cases)
        return sorted(set(deps))

    def add_field(self, field):
        # Did we get a field name instead of a field?
        if isinstance(field, str):
            if field in builtin_fields:
                return None
            elif field in self._fields.keys():
                # Field of this name already exists, no need to add it again
                return self._fields[field]
            else:
                # Create a proper field object from known field name with default params
                #field = field_classes[field](params=None)
                field = field_classes[field](params={"end_time":-1e16, "end_timestep": -1e16}) # ?Why not None?

        # Note: If field already exists, replace anyway to overwrite params, this
        # typically happens when a fields has been created implicitly by dependencies.
        # This is a bit unsafe though, the user might add a field twice with different parameters...
        # Check that at least the same name is not used for different field classes:
        assert type(field) == type(self._fields.get(field.name,field))

        # Analyse dependencies of field through magic
        deps = self.find_dependencies(field)

        # Add dependent fields to self._fields (this will add known fields by name)
        for depname in set(d[0] for d in deps) - set(self._fields.keys()):
            self.add_field(depname)

        # Build full dependency list
        full_deps = []
        existing_full_deps = set()
        for dep in deps:
            depname, ts = dep
            for fdep in self._full_dependencies[depname]:
                if fdep not in existing_full_deps:
                    existing_full_deps.add(fdep)
                    full_deps.append(fdep)
            existing_full_deps.add(dep)
            full_deps.append(dep)

        # Add field to internal data structures
        self._fields[field.name] = field
        self._dependencies[field.name] = deps
        self._full_dependencies[field.name] = full_deps
        self._insert_in_sorted_list(field.name)

        # Returning the field object is useful for testing
        return field

    def add_fields(self, fields):
        return [self.add_field(field) for field in fields]

    def should_compute_at_this_time(self, field, t, timestep):
        "Check if field is to be computed at current time"
        # If we later wish to move configuration of field compute frequencies to NSPostProcessor,
        # it's easy to swap here with e.g. fp = self._field_params[field.name]
        fp = field.params

        # Limit by timestep interval
        s = fp.start_timestep
        e = fp.end_timestep
        #if s > timestep or timestep > e:
        if not (s <= timestep <= e):
            return False

        # Limit by time interval
        s = fp.start_time
        e = fp.end_time
        #if s > t or t > e:
        eps = 1e-10
        if not (s-eps <= t <= e+eps):
            return False

        # Limit by frequency (accept if no previous data)
        pct, pcts = self._last_trigger_time[field.name]
        if timestep - pcts < fp.stride_timestep:
            return False
        if t - pct < fp.stride_time:
            return False

        # Accept!
        return True

    def get(self, name, timestep=0):
        """Get the value of a named field at a particular.

        The timestep is relative to now.
        Values are computed at first request and cached.
        """
        # Hack to access the spaces and problem arguments to update()
        spaces = self._spaces
        problem = self._problem

        # Check cache
        c = self._cache[timestep]
        data = c.get(name)

        # Cache miss?
        if data is None:
            if timestep == 0:
                # Ensure before_first_compute is always called once initially
                field = self._fields[name]
                if self._compute_counts[field.name] == 0:
                    init_data = field.before_first_compute(self, spaces, problem)
                    if init_data is not None and field.params["save"]:
                        self._init_metadata_file(name, init_data)

                # Compute value
                t0 = timeit()
                if not name in self._solution:
                    data = field.compute(self, spaces, problem)
                else:
                    data = field.convert(self, spaces, problem)
                self._compute_counts[field.name] += 1
                if self.params.enable_timer:
                    timeit(t0, "time to compute %s" % (name,))

                # Copy functions to avoid storing references to the same function objects at each timestep
                # NB! In other cases we assume that the fields return a new object for every compute!
                # Check first if we actually will cache this object by looking at 'time to keep' in the plan
                if self._plan[0][name] > 0:
                    if isinstance(data, Function):
                        # TODO: Use function pooling to avoid costly allocations?
                        data = Function(data)

                # Cache it!
                c[name] = data
            else:
                # Cannot compute missing value from previous timestep,
                # dependency handling must have failed
                raise DependencyException(name, timestep)

        return data

    def _apply_action(self, action, field, data):
        "Apply some named action to computed field data."
        if not field.name in self._cache[0]:
            error("Field '%s' is not in cache, this should not be possible." % field.name)

        t0 = timeit()
        if action == "save":
            self._action_save(field, data)

        elif action == "plot":
            self._action_plot(field, data)

        elif action == "callback":
            self._action_callback(field, data)

        else:
            error("Unknown action %s." % action)
        if self.params.enable_timer:
            timeit(t0, "time to %s %s" % (action, field.name))

    def _action_callback(self, field, data):
        "Apply the 'callback' action to computed field data."
        if callable(self._callback):
            self._callback(field, data, self.get("t"), self.get("timestep"))

    def _get_save_formats(self, field, data):
        if field.params.save_as == PPField.default_save_as():
            # Determine proper file formats from data type if not specifically provided
            if isinstance(data, Function):
                save_as = ['xdmf', 'hdf5']
            elif isinstance(data, (float, int, list, tuple, dict)):
                save_as = ['txt']
            else:
                error("Unknown data type %s, cannot determine file type automatically." % type(data).__name__)
        else:
            if isinstance(field.params.save_as, (list, tuple)):
                save_as = list(field.params.save_as)
            else:
                save_as = [field.params.save_as]
        return save_as

    def _get_casedir(self):
        return self.params.casedir

    def _create_casedir(self):
        casedir = self.params.casedir
        safe_mkdir(casedir)
        return casedir

    def _get_savedir(self, field_name):
        return os.path.join(self.params.casedir, field_name)

    def _create_savedir(self, field_name):
        self._create_casedir()
        savedir = self._get_savedir(field_name)
        safe_mkdir(savedir)
        return savedir

    """
    def _update_metadata_file(self, field_name, data, save_count, save_as, metadata):
        if on_master_node:
            savedir = self._get_savedir(field_name)
            metadata_filename = os.path.join(savedir, 'metadata.txt')

            # Create initial metadata file for first save call, or just append to the existing one
            if save_count == 0:
                # Initially just tell what data type this field has, and which formats we will save to
                metadata_file = open(metadata_filename, 'w')
                metadata_file.write('type=%r\n' % (type(data).__name__,))
                metadata_file.write('saveformats=%r\n' % (save_as,))

                # Then add type specific initial metadata
                if isinstance(data, Function):
                    # It's nice to have element data easily accessible
                    # TODO: Support metadescription of mesh regions? Boundary meshes? Will probably needed that for automated interpretation later.
                    metadata_file.write("element=%r\n" % (data.element(),))
                    metadata_file.write("element_degree=%r\n" % (data.element().degree(),))
                    metadata_file.write("element_family=%r\n" % (data.element().family(),))
                    metadata_file.write("element_value_shape=%r\n" % (data.element().value_shape(),))

                # TODO: Figure out and document what kind of txt data we support
                #elif isinstance(data, dict):
                #    metadata_file.write("keys=%r\n" % (sorted(data.keys()),))

                # Add a separator line between header and per-timestep data
                metadata_file.write('#'*40 + '\n')
                metadata_file.close()

            # Write metadata for this timestep
            assert all(isinstance(md, tuple) and len(md) == 2 for md in metadata)
            sep = "\t" # TODO: Separate with tabs or newlines?
            metadata_file = open(metadata_filename, 'a')
            metadata_file.writelines(sep.join("%s=%r" % (md[0], md[1]) for md in metadata))
            metadata_file.write('\n')
            metadata_file.close()
    """

    def _init_metadata_file(self, field_name, init_data):
        savedir = self._create_savedir(field_name)
        if on_master_node:
            metadata_filename = os.path.join(savedir, 'metadata.db')
            metadata_file = shelve.open(metadata_filename)
            metadata_file["init_data"] = init_data
            metadata_file.close()

    def _finalize_metadata_file(self, field_name, finalize_data):
        if on_master_node:
            savedir = self._get_savedir(field_name)
            metadata_filename = os.path.join(savedir, 'metadata.db')
            metadata_file = shelve.open(metadata_filename)
            metadata_file["finalize_data"] = finalize_data
            metadata_file.close()

    def _update_metadata_file(self, field_name, data, t, timestep, save_as, metadata):
        if on_master_node:
            savedir = self._get_savedir(field_name)
            metadata_filename = os.path.join(savedir, 'metadata.db')
            metadata_file = shelve.open(metadata_filename)

            # Store some data the first time
            if "type" not in metadata_file:
                # Data about type and formats
                metadata_file["type"] = type(data).__name__
                metadata_file["saveformats"] = list(set(save_as+metadata_file.get("saveformats", [])))
                # Data about function space
                if isinstance(data, Function):
                    metadata_file["element"] = repr(data.element(),)
                    metadata_file["element_degree"] = repr(data.element().degree(),)
                    metadata_file["element_family"] = repr(data.element().family(),)
                    metadata_file["element_value_shape"] = repr(data.element().value_shape(),)
            # Store some data each timestep
            metadata_file[str(timestep)] = metadata
            metadata_file[str(timestep)]["t"] = t

            # Flush file between timesteps
            metadata_file.close()

    def _get_datafile_name(self, field_name, saveformat, save_count):
        # These formats produce a new file each time
        counted_formats = ('xml', 'xml.gz')

        metadata = {}

        # Make filename, with or without save count in name
        if saveformat in counted_formats:
            filename = "%s%d.%s" % (field_name, save_count, saveformat)
            # If we have a new filename each time, store the name in metadata
            #metadata = [('filename', filename)]
            metadata['filename'] = filename

        else:
            filename = "%s.%s" % (field_name, saveformat)
            if saveformat == 'hdf5':
                #metadata = [('dataset', field_name+str(save_count))]
                metadata['dataset'] = field_name+str(save_count)

        savedir = self._get_savedir(field_name)
        fullname = os.path.join(savedir, filename)
        return fullname, metadata

    def _update_pvd_file(self, field_name, saveformat, data, save_count, t):
        assert isinstance(data, Function)
        assert saveformat == "pvd"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)
        key = (field_name, saveformat)
        datafile = self._datafile_cache.get(key)
        if datafile is None:
            datafile = File(fullname)
            self._datafile_cache[key] = datafile
        datafile << data
        return metadata

    def _update_xdmf_file(self, field_name, saveformat, data, save_count, t):
        assert isinstance(data, Function)
        assert saveformat == "xdmf"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)
        key = (field_name, saveformat)
        datafile = self._datafile_cache.get(key)
        if datafile is None:
            datafile = XDMFFile(fullname)
            datafile.parameters["rewrite_function_mesh"] = False
            datafile.parameters["flush_output"] = True
            self._datafile_cache[key] = datafile
        datafile << (data, t)
        return metadata

    def _update_hdf5_file(self, field_name, saveformat, data, save_count, t):
        assert saveformat == "hdf5"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)

        if save_count == 0:
            datafile = HDF5File(fullname, 'w')
            datafile.write(data, field_name+str(save_count))
            datafile.write(data.function_space().mesh(), "Mesh")
            del datafile
        else:
            datafile = HDF5File(fullname, 'a')
            datafile.write(data.vector(), field_name+str(save_count)+"/vector")
            del datafile

            # Create internal links in hdf5-file
            hdf5_link(fullname, field_name+"0"+"/x_cell_dofs", field_name+str(save_count)+"/x_cell_dofs")
            hdf5_link(fullname, field_name+"0"+"/cell_dofs", field_name+str(save_count)+"/cell_dofs")
            hdf5_link(fullname, field_name+"0"+"/cells", field_name+str(save_count)+"/cells")

        return metadata

    def _update_xml_file(self, field_name, saveformat, data, save_count, t):
        assert saveformat == "xml"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)
        datafile = File(fullname)
        datafile << data
        return metadata

    def _update_xml_gz_file(self, field_name, saveformat, data, save_count, t):
        assert saveformat == "xml.gz"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)
        datafile = File(fullname)
        datafile << data
        return metadata

    def _update_txt_file(self, field_name, saveformat, data, save_count, t):
        # TODO: Identify which more well defined data formats we need
        assert saveformat == "txt"
        fullname, metadata = self._get_datafile_name(field_name, saveformat, save_count)
        if on_master_node:
            if save_count == 0:
                datafile = open(fullname, 'w')
            else:
                datafile = open(fullname, 'a')
            datafile.write(str(data))
            datafile.write("\n")
            datafile.close()
        return metadata

    def store_params(self, params):
        casedir = self._create_casedir()

        pfn = os.path.join(casedir, "params.pickle")
        with open(pfn, 'w') as f:
            pickle.dump(params, f)

        tfn = os.path.join(casedir, "params.txt")
        with open(tfn, 'w') as f:
            f.write(str(params))

        #jfn = os.path.join(casedir, "params.json")
        #with open(jfn, 'w') as f:
        #    f.write(params.to_json())

    def _action_save(self, field, data):
        "Apply the 'save' action to computed field data."
        field_name = field.name

        # Create save folder first time
        save_count = self._save_counts[field_name]
        if save_count == 0:
            self._create_savedir(field_name)

        # Get current time (assuming the cache contains
        # valid 't' and 'timestep' at each step)
        t = self.get("t")
        timestep = self.get('timestep')

        # Get list of file formats
        save_as = self._get_save_formats(field, data)

        # Collect metadata shared between data types
        metadata = {
            'save_count': save_count,
            'timestep': timestep,
            'time': t,
            }

        # Rename Functions to get the right name in file
        # (NB! This has the obvious side effect!)
        # TODO: We don't need to cache a distinct Function
        # object like we do for plotting, or?
        if isinstance(data, Function):
            data.rename(field_name, "Function produced by cbcflow postprocessing.")

        # Write data to file for each filetype
        for saveformat in save_as:
            # Write data to file depending on type
            if saveformat == 'pvd':
                metadata[saveformat] = self._update_pvd_file(field_name, saveformat, data, save_count, t)
            elif saveformat == 'xdmf':
                metadata[saveformat] = self._update_xdmf_file(field_name, saveformat, data, save_count, t)
            elif saveformat == 'xml':
                metadata[saveformat] = self._update_xml_file(field_name, saveformat, data, save_count, t)
            elif saveformat == 'xml.gz':
                metadata[saveformat] = self._update_xml_gz_file(field_name, saveformat, data, save_count, t)
            elif saveformat == 'txt':
                metadata[saveformat] = self._update_txt_file(field_name, saveformat, data, save_count, t)
            elif saveformat == 'hdf5':
                metadata[saveformat] = self._update_hdf5_file(field_name, saveformat, data, save_count, t)
            else:
                error("Unknown save format %s." % (saveformat,))

        # Write new data to metadata file
        self._update_metadata_file(field_name, data, t, timestep, save_as, metadata)

        # Update save count
        self._save_counts[field_name] = save_count + 1

    def _action_plot(self, field, data):
        "Apply the 'plot' action to computed field data."
        if disable_plotting:
            return
        if isinstance(data, Function):
            if dolfin_plotting:
                self._plot_dolfin(field.name, data)
        elif isinstance(data, float):
            if pylab:
                self._plot_pylab(field.name, data)
        else:
            cbcflow_warning("Unable to plot object %s of type %s." % (field.name, type(data)))

    def _plot_dolfin(self, field_name, data):
        # Get current time
        t = self.get("t")
        timestep = self.get('timestep')

        # Plot or re-plot
        plot_object = self._plot_cache.get(field_name)
        if plot_object is None:
            plot_object = plot(data, title=field_name, **self._fields[field_name].params.plot_args)
            self._plot_cache[field_name] = plot_object
        else:
            plot_object.plot(data)

        # Set title and show
        title = "%s, t=%0.4g, timestep=%d" % (field_name, t, timestep)
        plot_object.parameters["title"] = title

    def _plot_pylab(self, field_name, data):
        # Hack to access the spaces and problem arguments to update()
        problem = self._problem

        # Get current time
        t = self.get("t")
        timestep = self.get('timestep')

        # Values to plot
        x = t
        y = data

        # Plot or re-plot
        plot_data = self._plot_cache.get(field_name)
        if plot_data is None:
            figure_number = len(self._plot_cache)
            pylab.figure(figure_number)

            xdata = [x]
            ydata = [y]
            newmin = min(ydata)
            newmax = max(ydata)

            plot_object, = pylab.plot(xdata, ydata)
            self._plot_cache[field_name] = plot_object, figure_number, newmin, newmax
        else:
            plot_object, figure_number, oldmin, oldmax = plot_data
            pylab.figure(figure_number)

            xdata = list(plot_object.get_xdata())
            ydata = list(plot_object.get_ydata())
            xdata.append(x)
            ydata.append(y)
            newmin = min(ydata)
            newmax = max(ydata)

            # Heuristics to avoid changing axis bit by bit, which results in fluttering plots
            # (Based on gut feeling, feel free to adjust these if you have a use case it doesnt work for)
            if newmin < oldmin:
                # If it has decreased, decrease by at least this factor
                #ymin = min(newmin, oldmin*0.8) # TODO: Negative numbers?
                ymin = newmin
            else:
                ymin = newmin
            if newmax > oldmax:
                # If it has increased, increase by at least this factor
                #ymax = max(newmax, oldmax*1.2) # TODO: Negative numbers?
                ymax = newmax
            else:
                ymax = newmax

            # Need to store min/max for the heuristics to work
            self._plot_cache[field_name] = plot_object, figure_number, ymin, ymax

            plot_object.set_xdata(xdata)
            plot_object.set_ydata(ydata)

            pylab.axis([problem.params.T0, problem.params.T, ymin, ymax])

        # Set title and show
        title = "%s, t=%0.4g, timestep=%d, min=%.2g, max=%.2g" % (field_name, t, timestep, newmin, newmax)
        plot_object.get_axes().set_title(title)
        pylab.xlabel("t")
        pylab.ylabel(field_name)
        pylab.draw()

    def _rollback_plan(self, t, timestep):
        # Roll plan one timestep and countdown how long to keep stuff
        tss = sorted(self._plan.keys())
        new_plan = defaultdict(lambda: defaultdict(int))

        # Countdown time to keep each data item and only keep what we still need
        for ts in tss:
            for name, ttk in self._plan[ts].items():
                if ttk > 0:
                    new_plan[ts-1][name] = ttk - 1
        self._plan = new_plan

    def _update_plan(self, t, timestep):
        # ttk = timesteps to keep
        #self._plan[-1][name] = ttk # How long to cache what's already computed
        #self._plan[0][name] = ttk  # What to compute now and how long to cache it
        #self._plan[1][name] = ttk  # What to compute in future and how long to cache it

        self._rollback_plan(t, timestep)

        # Loop over all fields that are triggered for computation at this timestep
        triggered_fields = [(name, field) for name, field in self._fields.iteritems()
                            if self.should_compute_at_this_time(field, t, timestep)]

        for name, field in triggered_fields:
            deps = self._full_dependencies[name]
            if deps:
                # Need to plan ahead this many steps (ts is non-positive)
                offset = abs(min(ts for depname, ts in deps))
                # Plan computation of dependenices:
                for depname, ts in deps:
                    # Store how long we need to cache this computation, highest of offset and old plan
                    oldttk = self._plan[ts+offset].get(depname, 0)
                    ttk = max(oldttk, offset)
                    self._plan[ts+offset][depname] = ttk
            else:
                # No planning ahead, compute right away
                offset = 0

            # Plan computation of this field:
            oldttk = self._plan[offset].get(name, 0)
            ttk = max(oldttk, offset)
            self._plan[offset][name] = ttk

            # Store compute trigger times to keep track of compute intervals
            self._last_trigger_time[field.name] = (t, timestep)

    def _execute_plan(self, t, timestep):
        # Initialize cache for current timestep
        assert not self._cache[0], "Not expecting cached computations at this timestep before plan execution!"
        self._cache[0] = {
            "t": t,
            "timestep": timestep,
            }
        # Loop over all planned field computations
        fields_to_compute = [name for name in self._sorted_fields_keys if name in self._plan[0]]
        for name in fields_to_compute:
            field = self._fields[name]
            # Execute computation through get call
            try:
                data = self.get(name)
            except DependencyException as e:
                cbcflow_warning(e.message)
                data = None

            # Apply action if it was triggered directly this timestep (not just indirectly)
            if (data is not None) and (self._last_trigger_time[name][1] == timestep):
                for action in ["save", "plot", "callback"]:
                    if field.params[action]:
                        self._apply_action(action, field, data)

        if 0: # Debugging:
            s1 = set(fields_to_compute)
            s2 = set(self._cache[0])
            s2.remove("t")
            s2.remove("timestep")
            s2m1 = s2 - s1
            s1m2 = s1 - s2
            if s2m1 or s1m2:
                print "\nCompute lists differ:"
                print sorted(s2m1) #s2-s1 = fields in s2 but not in s1, that is in expected cache but not in computation list
                print sorted(s1m2)
                print "Plan is:"
                print self._plan[0]
                print "Sorted field keys:"
                print self._sorted_fields_keys
                print "fields_to_compute:"
                print fields_to_compute
                print "cache[0]:"
                print self._cache[0]
                print
        assert len(fields_to_compute) == len(self._cache[0])-2, "This should hold if planning algorithm works properly?"

    def _update_cache(self):
        problem = self._problem

        new_cache = defaultdict(dict)
        # Loop over cache plans for each timestep
        for ts, plan in self._plan.iteritems():
            # Skip whats not computed yet
            if ts > 0:
                continue
            # Only keep what we have planned to cache
            for name, ttk in plan.iteritems():
                if ttk > 0:
                    # Cache should contain old cached values at ts<0 and newly computed values at ts=0
                    data = self._cache[ts].get(name)
                    assert data is not None, "Missing cache data!"
                    # Insert data in new cache at position ts-1
                    new_cache[ts-1][name] = data
        self._cache = new_cache

    def update_all(self, solution, t, timestep, spaces, problem):
        "Update all PPFields"

        # TODO: Better design solution to making these variables accessible the right places?
        self._problem = problem
        self._spaces = spaces
        self._solution = solution

        # Update cache to keep what's needed later according to plan, forget what we don't need
        self._update_cache()

        # Plan what we need to compute now and in near future based on action triggers and dependencies
        self._update_plan(t, timestep)

        # Compute what's needed according to plan
        self._execute_plan(t, timestep)

        # Reset hack to make these objects available throughout during update, to
        # make sure these objects are not referenced after this update call is over
        #self._problem = None
        #self._spaces = None
        #self._solution = None

        # FIXME: Tests are calling pp.get after update_all, this fails for two reasons:
        # - the hack reset above
        # - the cache rotation above (previously cache rotation was at the beginning of update_all)
        # We can't just use pp.get(name,-1) from the tests either, because that will not
        # allow new computations. Do we want to allow stand-alone get? Find a solution!
        # Probably involves updating cache at beginning, so we don't forget data until the
        # top of the next update_all call.

    def finalize_all(self, spaces, problem):
        "Finalize all PPFields after last timestep has been computed."
        for name in self._sorted_fields_keys:
            field = self._fields[name]
            finalize_data = field.after_last_compute(self, spaces, problem)
            if finalize_data is not None and field.params["save"]:
                self._finalize_metadata_file(name, finalize_data)
